import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {JwtHelperService} from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class AuthClienteGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token = localStorage.getItem('access_token');
    const helper = new JwtHelperService()
    if (token) {
      const jwtDecode = helper.decodeToken(token);
      console.log(!helper.isTokenExpired(token) && jwtDecode.role == "Cliente")
      if(!helper.isTokenExpired(token) && jwtDecode.role == "Cliente"){
        return  true;
      }else {
        this.redirectToLogin(state);
        return false;
      }
    }else {
      this.redirectToLogin(state);
      return false
    }
  }

  redirectToLogin(state:RouterStateSnapshot){
    this.router.navigate(['/login'], {
      queryParams: {
        return: state.url
      }
    });
    return false
  }
}
