import { Component, OnInit } from '@angular/core';
import {CarrinhoService} from "../../../services/carrinho.service";
import {Produto} from "../../../models/produto";
import {environment} from "../../../../environments/environment";
import {NzMessageService} from "ng-zorro-antd/message";
import {NzModalService} from "ng-zorro-antd/modal";
import {Router, RouterState, RouterStateSnapshot} from "@angular/router";
import {Carrinho} from "../../../models/carrinho";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.component.html',
  styleUrls: ['./carrinho.component.scss']
})
export class CarrinhoHomeComponent implements OnInit {
  carrinho:Carrinho;
  produtos:Produto[];
  state: RouterState;
  snapshot: RouterStateSnapshot;
  constructor(private carrinhoService:CarrinhoService,private message: NzMessageService,private modal: NzModalService,private router: Router,public  authService:AuthService) {
     this.state = router.routerState;
     this.snapshot = this.state.snapshot;
  }

  ngOnInit(): void {
    this.carrinhoService.carrinho.subscribe(carrinho => {
      this.carrinho = carrinho;
      this.carrinhoService.getPreviewCarrinho().subscribe(data =>{
        this.produtos = data.data;
      })
    })
  }

  getQuantidade(produto:Produto){
    if(!this.carrinho || !this.carrinho.carrinho)return 0;
    let index = this.carrinho.carrinho.findIndex(value => value.produto == produto.id);
    if(index != -1){
      return this.carrinho.carrinho[index].quantidade
    }
    return 0;
  }

  getSubTotal(produto:Produto){
    if(!this.carrinho || !this.carrinho.carrinho)return 0;
    let index = this.carrinho.carrinho.findIndex(value => value.produto == produto.id);
    if(index != -1){
      return this.carrinho.carrinho[index].quantidade * produto.valor
    }
    return 0;
  }

  getTotal(){
    if(!this.carrinho || !this.carrinho.carrinho)return 0;
    return this.carrinho.carrinho.reduce((previousValue, currentValue) => {
          let index = this.produtos.findIndex(value => value.id == currentValue.produto)
          return previousValue + this.produtos[index].valor * currentValue.quantidade;
    },0).toFixed(2)
  }

  redirectToLogin(){
    this.router.navigate(['/login'], {
      queryParams: {
        return: this.snapshot.url
      }
    });
    return false
  }


  getImageUrl(produto:Produto){
    return `${environment.API_URL}${produto.imagem}`
  }

  comprar() {
    this.modal.confirm({
      nzTitle: '<i>Finalizar essa compra?</i>',
      nzContent: `<b>Quantidade : ${this.produtos.length}</b>`,
      nzOnOk: () => {
        this.carrinhoService.saveProduto().subscribe(value => {
          this.message.info("Compra efetuada com sucesso!");
          this.carrinhoService.esvaziarCarrinho();
          this.router.navigate(['/']);
        })
      }})
  }

  limpar() {
    this.modal.confirm({
      nzTitle: '<i>Esvaziar essa carrinho?</i>',
      nzContent: `<b>Quantidade : ${this.produtos.length} Valor: ${this.getTotal()}</b>`,
      nzOnOk: () => {
        this.carrinhoService.esvaziarCarrinho();
        this.carrinhoService.esvaziarCarrinho();
        this.router.navigate(['/']);
      }})
  }
}
