import { Component, OnInit } from '@angular/core';
import {CategoriaService} from "../../../services/categoria.service";
import {Categoria} from "../../../models/categoria";
import {Produto} from "../../../models/produto";
import { ProdutoService } from 'src/app/services/produto.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-home-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexHomeComponent implements OnInit {

  public categorias:Categoria[];
  public produtos:Produto[];

  constructor(private categoriaService:CategoriaService,private produtoService:ProdutoService) { }


  ngOnInit(): void {
    this.categoriaService.getCategorias().subscribe(data =>{
      if(data.succeeded){
        this.categorias = data.data;
      }
    });
    this.produtoService.getProdutos(1,4).subscribe(data =>{
      if(data.succeeded){
        this.produtos = data.data;
      }
    });
  }

  getImageUrl(produto:Produto){
    return `${environment.API_URL}${produto.imagem}`
   }
  
}
