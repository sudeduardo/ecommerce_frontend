import { Component, OnInit } from '@angular/core';
import {CategoriaService} from "../../../services/categoria.service";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {Categoria} from "../../../models/categoria";
import {Produto} from "../../../models/produto";
import {chunk} from "lodash";
import {CarrinhoService} from "../../../services/carrinho.service";

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasHomeComponent implements OnInit {
  public chunk = chunk
  private loadProdutos: boolean;

  constructor(private categoriaService: CategoriaService, private route: ActivatedRoute, private router: Router,private carrinhoService:CarrinhoService) { }
  id: string | null;

  total = 0;
  pageSize = 10;
  pageIndex = 1;

  categoria: Categoria;
  produtos: Produto[];
  searchProdutdo: string = "";

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
      this.setCategory()
      this.setProdutoCategory();
    })
  }

  setCategory() {
    if (this.id) {
      this.categoriaService.getCategoriaById(this.id).subscribe(res => {
        if (res.succeeded) {
          this.categoria = res.data;

      }}, error => {
        console.log(error)
      })
    }
  }

  private setProdutoCategory() {
    this.loadDataFromServer(this.pageIndex, this.pageSize);
  }


  private loadDataFromServer(pageIndex: number = this.pageIndex, pageSize: number = this.pageSize) {
    if (this.id) {
      this.loadProdutos = true;
      this.categoriaService.getProdutosByCategoryId(this.id, pageIndex, pageSize, this.searchProdutdo).subscribe(data => {
        this.pageIndex = data.pageNumber;
        this.pageSize = data.pageSize;
        this.total = data.totalRecords;
        this.produtos = data.data;
        this.loadProdutos = false;
      });
    }
  }

  addProdutoToCarrinho(produto: Produto) {
    this.carrinhoService.addProduto(produto);
  }

  search() {
    this.loadDataFromServer();
  }
}
