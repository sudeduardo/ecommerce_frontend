import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Cliente} from "../../models/cliente";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  returnUrl: string;
  loading = false;
  submitted = false;
  error = '';
  constructor(private formBuilder: FormBuilder, private router: Router,private auth:AuthService, private route: ActivatedRoute,) {

  }

  ngOnInit() {
    if(this.auth.loggedIn){
      this.router.navigate(['/'])
    }

    var c = new Cliente();
    this.createForm(c);
    this.returnUrl = this.route.snapshot.queryParams['return'] || '/';
  }

  createForm(cliente: Cliente) {
    this.formLogin = this.formBuilder.group({
      email: [cliente.email, [Validators.required,Validators.email]],
      senha: [cliente.senha, [Validators.required,Validators.minLength]],
      remember:true
    })
  }

  onSubmit() {
    this.submitted = true;
    if (this.formLogin.valid) {
      let {email,senha} = this.formLogin.value;
      this.auth.login(email, senha)
        .pipe(first())
        .subscribe(
          () => this.router.navigate([this.returnUrl]),
          err => {
            this.error = err;
            this.loading = false;
          }
        );
    } else {
      Object.values(this.formLogin.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

}
