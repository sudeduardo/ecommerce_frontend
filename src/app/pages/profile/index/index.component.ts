import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {ClienteService} from "../../../services/cliente.service";
import {Token} from "../../../models/token";
import {Cliente} from "../../../models/cliente";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {NzMessageService} from "ng-zorro-antd/message";
import {Endereco} from "../../../models/endereco";

@Component({
  selector: 'app-profile-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexProfileComponent implements OnInit {
  public formEdit: FormGroup;

  private token: Token;
  public user: Cliente;
  public load: boolean = false;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private clienteService: ClienteService,private message: NzMessageService) {
  }

  ngOnInit() {

    this.authService.currentUser.subscribe(token => {
      this.token = token;
      this.loadData();
    });
  }

  public loadData() {
    this.load = true;
    if (this.token.nameid) {
      this.clienteService.getClienteById(this.token.nameid).subscribe(data => {
        this.load = false;
        this.user = data.data;
        this.createForm();

      })
    }
  }

  disabledDate = (date: Date): boolean => {
    return date > new Date();
  };


  createForm() {
    if(!this.user.endereco) this.user.endereco = new Endereco();
    this.formEdit = this.formBuilder.group({
      nome: [this.user.nome, [Validators.required, Validators.minLength(4)]],
      email: [this.user.email, [Validators.required, Validators.email]],
      dataNasc: [this.user.dataNasc, [Validators.required]],
      senha: [this.user.senha, [Validators.minLength(3),]],
      rua: [this.user.endereco.rua, [Validators.required]],
      cidade: [this.user.endereco.cidade, [Validators.required]],
      bairro: [this.user.endereco.bairro, [Validators.required]],
      complemento: [this.user.endereco.complemento, [Validators.required]],
      cep: [this.user.endereco.cep, [Validators.required]],
      numero: [this.user.endereco.numero, [Validators.required]],
    })
  }

  onSubmit() {
    if (this.formEdit.valid) {
      let {nome,email,dataNasc,senha,rua,cidade,bairro,complemento,numero,cep} = this.formEdit.value;

      this.user.nome = nome;
      this.user.email = email;
      this.user.dataNasc = dataNasc;
      this.user.senha = senha;
      this.user.endereco.rua = rua;
      this.user.endereco.cidade = cidade;
      this.user.endereco.bairro = bairro;
      this.user.endereco.complemento = complemento;
      this.user.endereco.numero = numero.toString();
      this.user.endereco.cep = cep;

      this.clienteService.updateCliente(this.user)
        .pipe(first())
        .subscribe(value => {
          if(value.succeeded){
            this.message.success("Dados pessoais atualizado com sucesso!",{nzDuration:30000})
            this.user = value.data;
            this.createForm()
          }else{
            this.message.error("Erro ao tentar atualizar seus dados tente mais tarde!")
          }
        });
    } else {
      Object.values(this.formEdit.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({onlySelf: true});
        }
      });
    }
  }
}
