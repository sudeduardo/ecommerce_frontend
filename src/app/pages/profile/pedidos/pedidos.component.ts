import { Component, OnInit } from '@angular/core';
import {ItemDeVenda, Venda} from "../../../models/venda";
import {NzTableQueryParams} from "ng-zorro-antd/table";
import * as moment from "moment";
import {VendaService} from "../../../services/venda.service";
import {NzModalService} from "ng-zorro-antd/modal";
import {Token} from "../../../models/token";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})
export class PedidosProfileComponent implements OnInit {

  vendas: Venda[];
  load: boolean = false;
  total = 0;
  pageSize = 10;
  pageIndex = 1;
  private user: Token;

  constructor(private vendaService: VendaService, private modalService: NzModalService, private authService: AuthService) {
    this.authService.currentUser.subscribe(x => {
      this.user = x
      this.loadDataFromServer();
    });
  }

  ngOnInit(): void {

  }

  loadDataFromServer(
    pageIndex: number = 1,
    pageSize: number = 10,
  ): void {
    this.load = true;
    this.vendaService.getVendasByUser(this.user.nameid,pageIndex, pageSize).subscribe(data => {
      this.load = false;
      this.pageIndex = data.pageNumber;
      this.pageSize = data.pageSize;
      this.total = data.totalRecords;
      this.vendas = data.data;
    });
  }

  onQueryParamsChange(params: NzTableQueryParams) {
    const {pageSize, pageIndex} = params;
    this.loadDataFromServer(pageSize,pageIndex);
  }

  onChange($event:any) {
    this.total = 0;
    this.pageSize = 10;
    this.pageIndex = 1;
    this.vendas = [];
    this.loadDataFromServer();
  }

  getTotalVenda(itens: ItemDeVenda[]) {
    return itens.reduce((previousValue, currentValue) => {
      return previousValue + currentValue.valorProduto;
    }, 0)
  }

  showProdutos(itemDeVendas: ItemDeVenda[]) {

    let items = itemDeVendas.reduce((previousValue, currentValue) => {
      return `${previousValue}
            <tr>
              <th><a href="/admin/produtos/editar/${currentValue.produto.id}">${currentValue.produto.titulo}</a></th>
              <th>R$ ${currentValue.valorProduto}</th>
            </tr>`
    }, "")
    let html = `<table>
                    <thead>
                      <tr>
                          <th>Produto</th>
                          <th>Valor na Compra</th>
                      </tr>
                    </thead>
                    <tbody>
                    ${items}
                    </tbody>
                </table>`
    this.modalService.info({
      nzTitle: 'Listagem de Produtos',
      nzContent: html,
      nzOkText: 'Fechar',
    });
  }

  formatDate(date: Date){
    return moment(date).format('DD/MM/YYYY HH:mm:ss');
  }

}
