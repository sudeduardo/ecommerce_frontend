import {Component, OnInit} from '@angular/core';
import {ItemDeVenda, Venda} from "../../../models/venda";
import {Produto} from "../../../models/produto";
import {NzTableQueryParams} from "ng-zorro-antd/table";
import {VendaService} from "../../../services/venda.service";
import {NzModalService} from "ng-zorro-antd/modal";
import * as moment from "moment";

@Component({
  selector: 'app-relatorios',
  templateUrl: './relatorios.component.html',
  styleUrls: ['./relatorios.component.scss']
})
export class RelatoriosComponent implements OnInit {

  constructor(private vendaService: VendaService, private modalService: NzModalService) {
  }

  date: Date[] = null;
  vendas: Venda[];
  load: boolean = false;
  total = 0;
  pageSize = 10;
  pageIndex = 1;

  ngOnInit(): void {
    this.loadDataFromServer();
  }

  loadDataFromServer(
    pageIndex: number = this.pageIndex,
    pageSize: number = this.pageSize,
  ): void {
    let [dataInicio,dataFim] = this.date || [null,null];
    this.load = true;
    this.vendaService.getVendas(pageIndex, pageSize, dataInicio, dataFim).subscribe(data => {
      this.load = false;
      this.pageIndex = data.pageNumber;
      this.pageSize = data.pageSize;
      this.total = data.totalRecords;
      this.vendas = data.data;
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    const {pageSize, pageIndex} = params;
    this.loadDataFromServer(pageIndex, pageSize);
  }

  onChange($event:any) {
    this.total = 0;
    this.pageSize = 10;
    this.pageIndex = 1;
    this.vendas = [];
    this.loadDataFromServer();
  }

  getTotalVenda(itens: ItemDeVenda[]) {
    return itens.reduce((previousValue, currentValue) => {
      return previousValue + currentValue.valorProduto;
    }, 0)
  }

  showProdutos(itemDeVendas: ItemDeVenda[]) {

    let items = itemDeVendas.reduce((previousValue, currentValue) => {
      return `${previousValue}
            <tr>
              <th><a href="/admin/produtos/editar/${currentValue.produto.id}">${currentValue.produto.titulo}</a></th>
              <th>R$ ${currentValue.valorProduto}</th>
            </tr>`
    }, "")
    let html = `<table>
                    <thead>
                      <tr>
                          <th>Produto</th>
                          <th>Valor na Compra</th>
                      </tr>
                    </thead>
                    <tbody>
                    ${items}
                    </tbody>
                </table>`
    this.modalService.info({
      nzTitle: 'Listagem de Produtos',
      nzContent: html,
      nzOkText: 'Fechar',
    });
  }

  formatDate(date: Date){
    return moment(date).format('DD/MM/YYYY HH:mm:ss');
  }
}
