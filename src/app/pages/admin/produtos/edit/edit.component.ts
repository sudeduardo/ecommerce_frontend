import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {date, RxwebValidators} from "@rxweb/reactive-form-validators";
import {Categoria} from "../../../../models/categoria";
import {NzMessageService} from "ng-zorro-antd/message";
import {ProdutoService} from "../../../../services/produto.service";
import {CategoriaService} from "../../../../services/categoria.service";
import {Produto} from "../../../../models/produto";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditProdutoComponent implements OnInit {

  imagePreview:any|null;
  formEdit: FormGroup;
  public seachTextCateogria: string = "";
  public categorias: Categoria[] | null;
  private id: string;

  constructor(private msg: NzMessageService, private formBuilder: FormBuilder, private produtoService: ProdutoService, private categoriaService: CategoriaService,private route: ActivatedRoute,private router: Router) { }

  produto : Produto | null;

  loadCategoria = false;

  totalPages = 0;
  pageSizeCategoria = 10;
  pageIndexCategoria = 1;

  nzFilterOption = () => true;

  ngOnInit(): void {
    this.setFormProduto();
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
      this.setProduto()
    })
  }

  setFormProduto(){
    this.formEdit = this.formBuilder.group({
      titulo: [this.produto?.titulo, [Validators.required]],
      descricao: [this.produto?.descricao, [Validators.required]],
      imagemFile: [null, [
        RxwebValidators.extension({extensions: ["jpeg", "jpg", 'png']})
      ]],
      valor: [this.produto?.valor, [Validators.required]],
      categoriaId: [this.produto?.categoriaId, [Validators.required]],
    })
  }
  submitForm() {
    if(this.produto){
      for (const i in this.formEdit.controls) {
        if (this.formEdit.controls.hasOwnProperty(i)) {
          this.formEdit.controls[i].markAsDirty();
          this.formEdit.controls[i].updateValueAndValidity();
        }
      }
      if (this.formEdit.valid) {
        this.produtoService.updateProduto(this.id,this.formEdit.value).subscribe((data) => {
          if(data.succeeded){
            this.msg.success("O produto foi atualizado com sucesso!");
          }else{
            this.msg.error("Erro ao atualizar o produto, tente novamente!");
          }
        }, error => {
          console.log(error)
        })
      }
    }

  }

  onFileSelect(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if(this.formEdit != null) {
        this.formEdit.get('imagemFile').setValue(file);
        this.imagePreview = URL.createObjectURL(file)
      }
    }else{
      this.imagePreview = null;
    }
  }

  laodMoreCategoria(): void {
    this.loadCategoria = true;
    this.categoriaService.getCategorias(this.pageIndexCategoria, this.pageSizeCategoria, this.seachTextCateogria).subscribe(data => {
      this.loadCategoria = false;
      this.pageIndexCategoria = data.pageNumber;
      this.pageSizeCategoria = data.pageSize;
      this.totalPages = data.totalPages;
      if (this.categorias != null) {
        this.categorias = [...this.categorias, ...data.data]
      } else {
        this.categorias = data.data;
      }
    });
  }

  loadOnScroll() {
    if (this.pageIndexCategoria < this.totalPages) {
      this.pageIndexCategoria++;
      this.laodMoreCategoria();
    }
  }

  formatterReais = (value: number): string => {
    if(value){
      return `R$ ${value.toString().replace(",",".")}`;
    }else{
      return `R$ `;
    }

  }

  parserReais = (value: string): string => {
    if(value) return value.replace('R$', '').replace(",",".").trim()
    return "";
  };


  searchCategoria(value: string) {
    this.seachTextCateogria = value;
    this.pageIndexCategoria = 1;
    this.categorias = null;
    this.laodMoreCategoria();
  }

  private setProduto() {
    if (this.id) {
      this.produtoService.getProdutoById(this.id).subscribe(res => {
        if (res.succeeded) {
          this.produto = res.data;
          if(res.data.categoriaId){
            this.categoriaService.getCategoriaById(res.data.categoriaId).subscribe(dataCategoria => {
              this.categorias = [dataCategoria.data];
              this.setFormProduto();
            })
          }else{
            this.setFormProduto();
          }

        } else this.redirectToProdutos()
      }, error => {
        this.redirectToProdutos()
      })
    } else this.redirectToProdutos()
  }

  private redirectToProdutos() {
     this.router.navigate(['admin/produtos'])
  }

  public imgUrl(file:string){
    return environment.API_URL + file;
  }
}
