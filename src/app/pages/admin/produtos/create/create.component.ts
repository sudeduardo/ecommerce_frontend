import {Component, EventEmitter, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzMessageService} from "ng-zorro-antd/message";
import {RxFormBuilder, RxwebValidators} from "@rxweb/reactive-form-validators";
import {ProdutoService} from "../../../../services/produto.service";
import en from "@angular/common/locales/en";
import {CategoriaService} from "../../../../services/categoria.service";
import {Categoria} from "../../../../models/categoria";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateProdutoComponent implements OnInit {

  public seachTextCateogria: string = "";
  public categorias: Categoria[] | null;

  constructor(private msg: NzMessageService, private formBuilder: FormBuilder, private produtoService: ProdutoService, private categoriaService: CategoriaService, private router: Router) {
  }

  loadCategoria = false;
  totalPages = 0;
  pageSizeCategoria = 10;
  pageIndexCategoria = 1;

  formCadastro: FormGroup;
  nzFilterOption = () => true;

  ngOnInit(): void {

    this.formCadastro = this.formBuilder.group({
      titulo: [null, [Validators.required]],
      descricao: [null, [Validators.required]],
      imagemFile: [null, [Validators.required,
        RxwebValidators.extension({extensions: ["jpeg", "jpg", 'png']})
      ]],
      valor: [null, [Validators.required]],
      categoriaId: [null, [Validators.required]],
    })
  }

  submitForm() {
    for (const i in this.formCadastro.controls) {
      if (this.formCadastro.controls.hasOwnProperty(i)) {
        this.formCadastro.controls[i].markAsDirty();
        this.formCadastro.controls[i].updateValueAndValidity();
      }
    }
    if (this.formCadastro.valid) {
      this.produtoService.saveProduto(this.formCadastro.value).subscribe((data) => {
        if(data.succeeded){
          this.msg.success("O produto foi cadastrado com sucesso!");
          this.router.navigate(["/admin/produtos/editar/",data.data.id])
        }else{
          this.msg.error("Erro ao cadastrar o produto, tente novamente!");
        }

      }, error => {
        console.log(error)
      })
    }
  }

  onFileSelect(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.formCadastro.get('imagemFile').setValue(file);
    }
  }

  laodMoreCategoria(): void {
    this.loadCategoria = true;
    this.categoriaService.getCategorias(this.pageIndexCategoria, this.pageSizeCategoria, this.seachTextCateogria).subscribe(data => {
      this.loadCategoria = false;
      this.pageIndexCategoria = data.pageNumber;
      this.pageSizeCategoria = data.pageSize;
      this.totalPages = data.totalPages;
      if (this.categorias != null) {
        this.categorias = [...this.categorias, ...data.data]
      } else {
        this.categorias = data.data;
      }
    });
  }

  loadOnScroll() {
    if (this.pageIndexCategoria < this.totalPages) {
      this.pageIndexCategoria++;
      this.laodMoreCategoria();
    }
  }

  formatterReais = (value: number): string => {
    if(value){
      return `R$ ${value.toString().replace(",",".")}`;
    }else{
      return `R$ `;
    }

  }

  parserReais = (value: string): string => {
    if(value) return value.replace('R$', '').replace(",",".").trim()
    return "";
  };


  searchCategoria(value: string) {
    this.seachTextCateogria = value;
    this.pageIndexCategoria = 1;
    this.categorias = null;
    this.laodMoreCategoria();
  }
}
