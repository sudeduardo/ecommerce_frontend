import { Component, OnInit } from '@angular/core';
import {ProdutoService} from "../../../../services/produto.service";
import {Categoria} from "../../../../models/categoria";
import {Produto} from "../../../../models/produto";
import {NzTableQueryParams} from "ng-zorro-antd/table";
import {NzModalService} from "ng-zorro-antd/modal";
import {NzMessageService} from "ng-zorro-antd/message";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexProdutoAdminComponent implements OnInit {

  constructor(private produtoService:ProdutoService,private modal: NzModalService, private message: NzMessageService) { }

  total = 0;
  pageSize = 10;
  pageIndex = 1;
  produtoTitulo:string|null;
  produtos: Produto[];

  load = true;
  ngOnInit(): void {
    this.loadDataFromServer(this.pageIndex, this.pageSize)
  }

  loadDataFromServer(
    pageIndex: number = this.pageIndex,
    pageSize: number = this.pageSize,
  ): void {

    this.load = true;
    this.produtoService.getProdutos(pageIndex, pageSize,this.produtoTitulo).subscribe(data => {
      this.load = false;
      this.pageIndex = data.pageNumber;
      this.pageSize = data.pageSize;
      this.total = data.totalRecords;
      this.produtos = data.data;
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    const {pageSize, pageIndex} = params;
    this.loadDataFromServer(pageIndex, pageSize);
  }

  searchProduto() {
    this.total = 0;
    this.pageSize = 10;
    this.pageIndex = 1;
    this.produtos = [];
    this.loadDataFromServer();
  }

  confirmDeleteProduto(produto:Produto){
    this.modal.confirm({
      nzTitle: '<i>Vocẽ quer realmente deletar este produto?</i>',
      nzContent: `<b>${produto.titulo}</b>`,
      nzOnOk: () => {
        this.produtoService.deleteProduto(produto).subscribe(data => {
          console.log("Passei aqui")
          if (data.succeeded) {
            this.message.success(`O produto ${produto.titulo} foi removida com sucesso!`)
            this.loadDataFromServer();
          } else {
            this.message.error(`O produto ${produto.titulo} não pode ser removida com sucesso!`)
          }
        })
      }
    });
  }
}
