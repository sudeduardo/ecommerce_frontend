import {Component, OnInit} from '@angular/core';
import {CategoriaService} from "../../../../services/categoria.service";
import {Categoria} from "../../../../models/categoria";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {isNumeric} from "rxjs/internal-compatibility";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Produto} from "../../../../models/produto";
import {NzTableQueryParams} from "ng-zorro-antd/table";
import {NzModalService} from "ng-zorro-antd/modal";
import {NzMessageService} from "ng-zorro-antd/message";
import {merge} from "rxjs";
import {join} from "@angular/compiler-cli/src/ngtsc/file_system";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditCategoriaComponent implements OnInit {
  loadEdit = false;
  loadDelete = false;

  id: string | null;
  total = 0;
  pageSize = 10;
  pageIndex = 1;

  formEditCategory: FormGroup;
  categoria: Categoria;
  produtos: Produto[];
  loadProdutos = true;

  loadProdutosSearch = true;
  produtosSearch: Produto[];
  searchInput: string | null
  totalSearchProdutos = 0;
  pageSizeSearchProdutos = 10;
  pageIndexSearchProdutos = 1;


  nzFilterOption = () => true;

  selectedProductAdd: Produto | null;

  constructor(private categoriaService: CategoriaService, private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder, private modal: NzModalService, private message: NzMessageService,) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
      this.setCategory()
      this.setProdutoCategory();
    })

  }

  setCategory() {
    if (this.id) {
      this.categoriaService.getCategoriaById(this.id).subscribe(res => {
        console.log(res);
        if (res.succeeded) {
          this.categoria = res.data;
          this.createForm()
        } else this.redirectToCategory()
      }, error => {
        this.redirectToCategory()
      })
    } else this.redirectToCategory()
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    const {pageSize, pageIndex} = params;
    this.loadDataFromServer(pageIndex, pageSize);
  }

  loadDataFromServer(pageIndex: number, pageSize: number): void {
    if (this.id) {
      this.loadProdutos = true;
      this.categoriaService.getProdutosByCategoryId(this.id, pageIndex, pageSize).subscribe(data => {
        this.pageIndex = data.pageNumber;
        this.pageSize = data.pageSize;
        this.total = data.totalRecords;
        this.produtos = data.data;
        this.loadProdutos = false;
      });
    }
  }

  setProdutoCategory() {
    this.loadDataFromServer(this.pageIndex, this.pageSize);
  }

  redirectToCategory() {
    this.router.navigate(['admin/categorias'])
  }

  createForm() {
    this.formEditCategory = this.formBuilder.group({
      nome: [this.categoria.nome, [Validators.required, Validators.minLength(4)]],
    })
  }

  editCategory() {
    if (this.formEditCategory.valid) {
      this.loadEdit = true;
      let {nome} = this.formEditCategory.value;
      let categoria = new Categoria();
      categoria.nome = nome;
      this.categoriaService.saveCategoria(categoria).subscribe(data => {
          this.loadEdit = false;
          if (data.succeeded) {
           this.categoria = data.data;
            this.message.success("Categoria Atualizada com sucesso!");
          } else {
            let txt = data.errors.reduce((previousValue, currentValue) => `${previousValue} \n ${currentValue}`)
            this.message.error(txt);
          }

        },
        error => {
          this.message.error(`Não foi possivel atulizar está categoria!`)
        })
    } else {
      Object.values(this.formEditCategory.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({onlySelf: true});
        }
      });
    }
  }

  confirmDeleteCategoria(e: any): void {
    e.preventDefault();
    this.loadDelete = true;
    this.modal.confirm({
      nzTitle: '<i>Vocẽ quer realmente deletar esta categoria?</i>',
      nzContent: `<b>${this.categoria.nome}</b>`,
      nzOnOk: () => {
        this.categoriaService.deleteCategoria(this.categoria).subscribe(data => {
          this.loadDelete = false;
          if (data.succeeded) {
            this.message.success(`A categoria ${this.categoria.nome} foi removida com sucesso!`,{nzDuration:1000}).onClose.subscribe(e =>{
              this.router.navigate(['admin/categorias'])
            })
          } else {
            this.message.error(`A categoria ${this.categoria.nome} não pode ser removida com sucesso!`)
          }
        },error => {
          this.loadDelete = false;
          this.message.error(`A categoria ${this.categoria.nome} não pode ser removida com sucesso!`)
        })
      },
      nzOnCancel: () => {
        this.loadDelete = false
      }
    });
  }

  confirmRemoveProdutrofromCategoria(produto: Produto) {
    this.modal.confirm({
      nzTitle: `<i>Vocẽ quer realmente remover este produto da categoria${this.categoria.nome} ?</i>`,
      nzContent: `<b>${produto.titulo}</b>`,
      nzOnOk: () => {
        this.categoriaService.removeProdutoFromCategoria(this.categoria.id, produto.id).subscribe(data => {
          if (data.succeeded) {
            this.message.success(`O produto ${produto.titulo} foi removido da categoria ${this.categoria.nome} foi removida com sucesso!`)
            this.setProdutoCategory();
          } else {
            this.message.error(`O produto ${produto.titulo} não foi removido da categoria ${this.categoria.nome} com sucesso!`)
          }
        }, error => {
          this.message.error(`O produto ${produto.titulo} não foi removido da categoria ${this.categoria.nome}!`)
        })
      }
    });
  }

  confirmAddProdutroToCategoria() {
    if (this.selectedProductAdd != null) {
      this.modal.confirm({
        nzTitle: `<i>Vocẽ quer realmente adicionar este produto na categoria ${this.categoria.nome} ?</i>`,
        nzContent: `<b>${this.selectedProductAdd.titulo}</b>`,
        nzOnOk: () => {
          this.categoriaService.addProdutoToCategoria(this.categoria.id, this.selectedProductAdd.id).subscribe(data => {
            if (data.succeeded) {
              this.message.success(`O produto ${this.selectedProductAdd.titulo} foi adicionado da categoria ${this.categoria.nome} com sucesso!`)
              this.setProdutoCategory();
              this.selectedProductAdd = null;
            } else {
              this.message.error(`O produto ${this.selectedProductAdd.titulo} não foi adicionado a categoria ${this.categoria.nome} com sucesso!`)
            }
          }, error => {
            this.message.error(`O produto ${this.selectedProductAdd.titulo} não foi adicionado a categoria ${this.categoria.nome}!`)
          })
        }
      });
    } else {
      this.message.info("Nenhum produto foi selecionado!");
    }
  }

  searchProdutoToAdd(value: string|null) {
    this.searchInput = value;
    this.pageIndexSearchProdutos = 1;
    this.produtosSearch = null;
    this.laodMoreProdutos();
  }

  laodMoreProdutos(): void {
    this.loadProdutosSearch = true;
    this.categoriaService.getProdutosWithOutCategoria(this.searchInput,this.pageIndexSearchProdutos, this.pageSizeSearchProdutos).subscribe(data => {
      this.loadProdutosSearch = false;
      this.pageIndexSearchProdutos = data.pageNumber;
      this.pageSizeSearchProdutos = data.pageSize;
      this.totalSearchProdutos = data.totalPages;
      if (this.produtosSearch != null) {
        this.produtosSearch = [...this.produtosSearch, ...data.data]
      } else {
        this.produtosSearch = data.data;
      }
    });
  }

  loadOnScroll() {
    if (this.pageIndexSearchProdutos < this.totalSearchProdutos) {
      this.pageIndexSearchProdutos++;
      this.laodMoreProdutos();
    }
  }
}
