import {Component, OnInit} from '@angular/core';
import {CategoriaService} from "../../../../services/categoria.service";

import {Pagination} from "../../../../models/response/pagination";
import {Categoria} from "../../../../models/categoria";
import {NzTableQueryParams} from "ng-zorro-antd/table";
import {NzModalService} from "ng-zorro-antd/modal";
import {NzMessageService} from "ng-zorro-antd/message";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {Router} from "@angular/router";

@Component({
  selector: 'app-index-categorias',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexCategoriasComponent implements OnInit {

  formPostCategoria: FormGroup;
  total = 0;
  pageSize = 10;
  pageIndex = 1;

  categorias: Categoria[];

  load = true;
  loadBtnPost = false;

  constructor(private categoriaService: CategoriaService, private modal: NzModalService, private message: NzMessageService, private formBuilder: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
    this.loadDataFromServer(this.pageIndex, this.pageSize)

    this.formPostCategoria = this.formBuilder.group({
      nome: [null, [Validators.required, Validators.minLength(4)]]
    })
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    const {pageSize, pageIndex} = params;
    this.loadDataFromServer(pageIndex, pageSize);
  }

  loadDataFromServer(
    pageIndex: number = this.pageIndex,
    pageSize: number = this.pageSize,
  ): void {
    this.load = true;
    this.categoriaService.getCategorias(pageIndex, pageSize).subscribe(data => {
      this.load = false;
      this.pageIndex = data.pageNumber;
      this.pageSize = data.pageSize;
      this.total = data.totalRecords;
      this.categorias = data.data;
    });
  }

  confirmDeleteCategoria(categoria: Categoria): void {
    this.modal.confirm({
      nzTitle: '<i>Vocẽ quer realmente deletar esta categoria?</i>',
      nzContent: `<b>${categoria.nome}</b>`,
      nzOnOk: () => {
        this.categoriaService.deleteCategoria(categoria).subscribe(data => {
          if (data.succeeded) {
            this.message.success(`A categoria ${categoria.nome} foi removida com sucesso!`)
            this.loadDataFromServer();
          } else {
            this.message.error(`A categoria ${categoria.nome} não pode ser removida com sucesso!`)
          }
        })
      }
    });
  }

  createCategoria() {
    if (this.formPostCategoria.valid) {
      this.loadBtnPost = true;
      let {nome} = this.formPostCategoria.value;
      let categoria = new Categoria();
      categoria.nome = nome;
      this.categoriaService.saveCategoria(categoria).subscribe(data => {
          this.loadBtnPost = false;
          if (data.succeeded) {
            this.editCategoria(data.data);
          } else {
            let txt = data.errors.reduce((previousValue, currentValue) => `${previousValue} \n ${currentValue}`)
            this.message.error(txt);
          }

        },
        error => {
          console.log(error);
        })
    } else {
      Object.values(this.formPostCategoria.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({onlySelf: true});
        }
      });
    }
  }

  editCategoria(categoria: Categoria) {
    this.router.navigate([`admin/categorias/editar/${categoria.id}`])
  }


}
