import { Component, OnInit } from '@angular/core';
import {Cliente} from "../../../../../models/cliente";
import {NzTableQueryParams} from "ng-zorro-antd/table";
import {Categoria} from "../../../../../models/categoria";
import {NzModalService} from "ng-zorro-antd/modal";
import {NzMessageService} from "ng-zorro-antd/message";
import {MasterService} from "../../../../../services/master.service";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexUsuarioAdminComponent implements OnInit {

  constructor(private modal: NzModalService, private message: NzMessageService, private masterService:MasterService) { }

  ngOnInit(): void {
    this.loadDataFromServer(this.pageIndex, this.pageSize)
  }
  public usuarios:Cliente[];

  load:boolean = false;
  total = 0;
  pageSize = 10;
  pageIndex = 1;

  onQueryParamsChange($event: NzTableQueryParams) {

  }

  confirmDeleteUsurioMaster(cliente: Cliente) {
      this.modal.confirm({
        nzTitle: '<i>Vocẽ quer realmente deletar este usuario?</i>',
        nzContent: `<b>${cliente.nome}</b>`,
        nzOnOk: () => {
          this.masterService.deleteMaster(cliente).subscribe(data => {
            if (data.succeeded) {
              this.message.success(`O usuario ${cliente.nome} foi removido com sucesso!`)
              this.loadDataFromServer(this.pageIndex, this.pageSize);
            } else {
              this.message.error(`O usuairo ${cliente.nome} não pode ser removido com sucesso!`)
            }
          })
        }
      });
  }

  private loadDataFromServer(pageIndex: number, pageSize: number) {
    this.load = true;
    this.masterService.getMasters(pageIndex, pageSize).subscribe(data => {
      this.load = false;
      this.pageIndex = data.pageNumber;
      this.pageSize = data.pageSize;
      this.total = data.totalRecords;
      this.usuarios = data.data;
    });
  }
}
