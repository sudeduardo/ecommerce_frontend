import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MasterService} from "../../../../../services/master.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateUsuarioMasterComponent implements OnInit {

  formUserMaster!: FormGroup;

  submitForm(): void {
    for (const i in this.formUserMaster.controls) {
      if (this.formUserMaster.controls.hasOwnProperty(i)) {
        this.formUserMaster.controls[i].markAsDirty();
        this.formUserMaster.controls[i].updateValueAndValidity();
      }
    }
    console.log(this.formUserMaster.valid)
    if(this.formUserMaster.valid){
     this.masterService.saveMaster(this.formUserMaster.value).subscribe(data =>{
        this.router.navigate(['admin/usuarios/master'])
     });
    }

  }

  constructor(private fb: FormBuilder, private masterService:MasterService, private router: Router) {}

  ngOnInit(): void {
    this.formUserMaster = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      nome: [null, [Validators.required]],
      cpf: [null, [Validators.required]],
      senha: [null, [Validators.required]],
    });
  }

}
