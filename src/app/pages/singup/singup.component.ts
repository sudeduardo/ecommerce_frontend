import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {NzFormTooltipIcon} from "ng-zorro-antd/form";
import {ClienteService} from "../../services/cliente.service";
import {first} from "rxjs/operators";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.scss']
})
export class SingupComponent implements OnInit {
  validateForm!: FormGroup;

  captchaTooltipIcon: NzFormTooltipIcon = {
    type: 'info-circle',
    theme: 'twotone'
  };

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    if(this.validateForm.valid){
      if(this.validateForm.value.agree){
        this.cliente.saveCliente(this.validateForm.value).pipe(first())
          .subscribe(
            value => {
              this.auth.setToken(value.token);
              this.router.navigate(['/profile/index'])
            },
            err => console.log(err)
          );
      }else{

      }
    }

  }

  updateConfirmValidator(): void {
    Promise.resolve().then(() => this.validateForm.controls.senhaConfirmacao.updateValueAndValidity());
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls.senhaConfirmacao.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  constructor(private fb: FormBuilder, private cliente:ClienteService,private auth:AuthService, private router: Router) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      nome: [null, [Validators.required]],
      cpf: [null, [Validators.required]],
      dataNasc: [null, [Validators.required]],
      senha: [null, [Validators.required]],
      senhaConfirmacao: [null, [Validators.required, this.confirmationValidator]],
      agree: [false]
    });
  }
}
