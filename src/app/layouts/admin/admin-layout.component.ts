import { Component,OnInit } from '@angular/core';
import { faListOl,faBoxes,faFileAlt, faUserAlt} from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-home-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css']
})
export class AdminLayoutComponent {
  faListOl = faListOl;
  faBoxes = faBoxes;
  faFileAlt = faFileAlt;
  faUserAlt = faUserAlt;

  public isCollapsed = false

  constructor() {

  }

  ngOnInit() {
  }
}
