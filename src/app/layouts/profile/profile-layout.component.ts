import { Component, OnInit } from '@angular/core';
import { faListOl,faBoxes,faFileAlt, faUserAlt} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-profile-layout',
  templateUrl: './profile-layout.component.html',
  styleUrls: ['./profile-layout.component.css']
})
export class ProfileLayoutComponent {

  faBoxes = faBoxes;
  faUserAlt = faUserAlt;

  public isCollapsed = false

  constructor() {

  }

  ngOnInit() {
  }
}
