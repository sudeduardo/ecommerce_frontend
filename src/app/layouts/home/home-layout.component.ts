import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Token} from "../../models/token";
import {Router} from "@angular/router";
import {Categoria} from "../../models/categoria";
import {CategoriaService} from "../../services/categoria.service";

@Component({
  selector: 'app-login-layout',
  templateUrl: './home-layout-component.html',
  styleUrls: ['./home-layout.component.css']
})
export class HomeLayoutComponent implements OnInit {
  user: Token;
  constructor( private authService: AuthService, private router: Router,private categoriaService:CategoriaService) {
    this.authService.currentUser.subscribe(x => {
      this.user = x
    });
  }
  visible: boolean;
  categorias:Categoria[];
  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
    this.categoriaService.getCategorias().subscribe(data =>{
      if(data.succeeded){
        this.categorias = data.data;
      }
    });
  }

}
