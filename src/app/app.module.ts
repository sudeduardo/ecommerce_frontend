import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NZ_I18N} from 'ng-zorro-antd/i18n';
import {pt_BR} from 'ng-zorro-antd/i18n';
import {registerLocaleData} from '@angular/common';
import pt from '@angular/common/locales/pt';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {IconsProviderModule} from './icons-provider.module';
import {NzLayoutModule} from 'ng-zorro-antd/layout';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {NzCardModule} from 'ng-zorro-antd/card';
import {NzFormModule} from 'ng-zorro-antd/form';
import {LoginComponent} from './pages/login/login.component';
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzWaveModule} from "ng-zorro-antd/core/wave";
import {NzButtonModule} from "ng-zorro-antd/button";
import {JwtModule} from '@auth0/angular-jwt';

import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import {SingupComponent} from './pages/singup/singup.component';
import {AuthMasterGuard} from "./guard/auth-master.guard";
import {AuthClienteGuard} from "./guard/auth-cliente.guard";
import {AuthService} from "./services/auth.service";
import {ClienteService} from "./services/cliente.service";
import {IndexHomeComponent } from './pages/home/index/index.component';
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {AdminLayoutComponent} from "./layouts/admin/admin-layout.component";
import {HomeLayoutComponent} from "./layouts/home/home-layout.component";
import {LoginLayoutComponent} from "./layouts/login/login-layout.component";
import {ProfileLayoutComponent} from "./layouts/profile/profile-layout.component";
import {IndexAdminComponent } from "./pages/admin/index/index.component";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IndexCategoriasComponent } from './pages/admin/categorias/index/index.component';
import { RelatoriosComponent } from './pages/admin/relatorios/relatorios.component';
import {CategoriaService} from "./services/categoria.service";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzMessageModule} from "ng-zorro-antd/message";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzBreadCrumbModule} from "ng-zorro-antd/breadcrumb";
import { EditCategoriaComponent } from './pages/admin/categorias/edit/edit.component';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { CardProdutoComponent } from './components/card-produto/card-produto.component';
import {IndexProfileComponent} from "./pages/profile/index/index.component";
import { IndexProdutoAdminComponent } from './pages/admin/produtos/index/index.component';
import { CreateProdutoComponent } from './pages/admin/produtos/create/create.component';
import {NzUploadModule} from "ng-zorro-antd/upload";
import {  RxReactiveFormsModule } from "@rxweb/reactive-form-validators"
import {ProdutoService} from "./services/produto.service";
import { EditProdutoComponent } from './pages/admin/produtos/edit/edit.component';
import {NzImageModule} from "ng-zorro-antd/experimental/image";
import {IndexUsuarioAdminComponent} from "./pages/admin/usuarios/master/index/index.component";
import { CreateUsuarioMasterComponent } from './pages/admin/usuarios/master/create/create.component';
import {MasterService} from "./services/master.service";
import { PedidosProfileComponent } from './pages/profile/pedidos/pedidos.component';
import { CategoriasHomeComponent } from './pages/home/categorias/categorias.component';
import {CarrinhoService} from "./services/carrinho.service";
import {NzPageHeaderModule} from "ng-zorro-antd/page-header";
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {VendaService} from "./services/venda.service";
import { MenuProfileComponent } from './components/menu-profile/menu-profile.component';
import { CartComponent } from './components/cart/cart.component';
import {NzBadgeModule} from "ng-zorro-antd/badge";
import { CarrinhoHomeComponent } from './pages/home/carrinho/carrinho.component';
import {NzListModule} from "ng-zorro-antd/list";
import {environment} from "../environments/environment";
import {InterceptTokenService} from "./services/intercept-token.service";

registerLocaleData(pt);

export function tokenGetter() {
  console.log("passou aqui")
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    IndexProfileComponent,
    IndexAdminComponent,
    AppComponent,
    LoginComponent,
    SingupComponent,
    IndexHomeComponent,
    AdminLayoutComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    ProfileLayoutComponent,
    IndexCategoriasComponent,
    RelatoriosComponent,
    EditCategoriaComponent,
    CardProdutoComponent,
    IndexProdutoAdminComponent,
    CreateProdutoComponent,
    EditProdutoComponent,
    IndexUsuarioAdminComponent,
    CreateUsuarioMasterComponent,
    PedidosProfileComponent,
    CategoriasHomeComponent,
    MenuProfileComponent,
    CartComponent,
    CarrinhoHomeComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        BrowserAnimationsModule,
        IconsProviderModule,
        NzLayoutModule,
        NzMenuModule,
        NzCardModule,
        NzFormModule,
        NzGridModule,
        NzInputModule,
        NzCheckboxModule,
        NzWaveModule,
        ReactiveFormsModule,
        NzButtonModule,
        NzModalModule,
        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                allowedDomains: [environment.API_URL],
            },
        }),
        NzSelectModule,
        NzDatePickerModule,
        FontAwesomeModule,
        NzTableModule,
        NzDividerModule,
        NzMessageModule,
        NzBreadCrumbModule,
        NzSpinModule,
        NzUploadModule,
        RxReactiveFormsModule,
        NzInputNumberModule,
        NzImageModule,
        NzPageHeaderModule,
        NzAvatarModule,
        NzDropDownModule,
        NzBadgeModule,
        NzListModule,
        NzImageModule,
        HttpClientModule,
    ],
  providers: [
    {provide: NZ_I18N, useValue: pt_BR},
    AuthMasterGuard,
    AuthClienteGuard,
    AuthService,
    ClienteService,
    CategoriaService,
    ProdutoService,
    MasterService,
    CarrinhoService,
    VendaService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptTokenService,
      multi: true
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
