import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {map} from 'rxjs/operators';
import {environment} from "../../environments/environment";
import {JwtHelperService} from "@auth0/angular-jwt";
import {Token} from "../models/token";

@Injectable({
  providedIn: 'root'
})

@Injectable({ providedIn: 'root' })
export class AuthService {

  private currentUserSubject: BehaviorSubject<Token>;

  public currentUser: Observable<Token>;

  constructor(private http: HttpClient) {
    const helper = new JwtHelperService()
    if(this.loggedIn){
      this.currentUserSubject = new BehaviorSubject<Token>(helper.decodeToken(localStorage.getItem('access_token')));
    }else{
      this.currentUserSubject = new BehaviorSubject<Token>(null);
    }
    this.currentUser = this.currentUserSubject.asObservable();

  }

  url = environment.API_URL

  public get getUser(): Token {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string): Observable<boolean> {
    return this.http.post<{token: string}>(`${this.url}/Auth/login`, {email: username, senha: password})
      .pipe(
        map(result => {
          this.setToken(result.token)
          return true;
        })
      );
  }

  getToken(){
    return localStorage.getItem('access_token');
  }
  setToken(token:string){
    const helper = new JwtHelperService()
    localStorage.setItem('access_token', token);
    this.currentUserSubject.next(helper.decodeToken(token));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('access_token');
    this.currentUserSubject.next(null);
  }

  public get loggedIn(): boolean {
    const helper = new JwtHelperService()
    let token = localStorage.getItem('access_token')
    return (token !== null) && !helper.isTokenExpired(token);
  }
}
