import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {retry, catchError} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {Categoria} from "../models/categoria";
import {Pagination} from "../models/response/pagination";
import {Response} from "../models/response/response";
import {Produto} from "../models/produto";

@Injectable()
export class CategoriaService {

  url = environment.API_URL

  // injetando o HttpClient
  constructor(private httpClient: HttpClient) {
  }

  // Headers
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  }

  getCategorias(
    pageIndex: number = 1,
    pageSize: number = 10,
    nome:string|null = null
  ): Observable<Pagination<Categoria>> {

    let params = new HttpParams()
      .append('PageNumber', `${pageIndex}`)
      .append('PageSize', `${pageSize}`)

    return this.httpClient
      .get<Pagination<Categoria>>(`${this.url}/Categoria${nome ? `/${nome}`: ''}`, {params})
      .pipe(retry(2),
        catchError(this.handleError));
  }

  getProdutosByCategoryId(
    id:string,
    pageIndex: number = 1,
    pageSize: number = 10,
    titulo:string|null = null
  ): Observable<Pagination<Produto>> {
    let params = new HttpParams()
      .append('PageNumber', `${pageIndex}`)
      .append('PageSize', `${pageSize}`)

    return this.httpClient
      .get<Pagination<Produto>>(`${this.url}/Categoria/${id}/Produtos${titulo ? `/${titulo}`: ''}`, {params})
      .pipe(retry(2),
        catchError(this.handleError));
  }

  getCategoriaById(id: string|number): Observable<Response<Categoria>> {
    return this.httpClient.get<Response<Categoria>>(`${this.url}/Categoria/${id}`)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  removeProdutoFromCategoria(id: number, produto: number){
    console.log(`${this.url}/Categoria/${id}/Produtos/${produto}`)
    return this.httpClient
      .delete<Response<string>>(`${this.url}/Categoria/${id}/Produtos/${produto}`)
      .pipe(retry(2),
        catchError(this.handleError));
  }

  saveCategoria(categoria: Categoria): Observable<Response<Categoria>> {
    return this.httpClient.post<Response<Categoria>>(`${this.url}/Categoria`, JSON.stringify(categoria), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  getProdutosWithOutCategoria(nome:string|null, pageIndex: number = 1,
                              pageSize: number = 10,): Observable<Pagination<Produto>> {
    let params = new HttpParams()
      .append('PageNumber', `${pageIndex}`)
      .append('PageSize', `${pageSize}`)

    return this.httpClient.get<Pagination<Produto>>(`${this.url}/Categoria/Produtos/SemCategoria${(nome)? `/${nome}`:''}`,{params})
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  updateCategoria(cliente: Categoria): Observable<Categoria> {
    return this.httpClient.put<Categoria>(`${this.url}/Categoria/${cliente.id}`, JSON.stringify(cliente), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  deleteCategoria(categoria: Categoria) {
    return this.httpClient.delete<Response<string>>(`${this.url}/Categoria/${categoria.id}`, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };

  addProdutoToCategoria(id: number, produto: number) {
    return this.httpClient
      .post<Response<string>>(`${this.url}/Categoria/${id}/Produtos/${produto}`,null,this.httpOptions)
      .pipe(retry(2),
        catchError(this.handleError));
  }
}
