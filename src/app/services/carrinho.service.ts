import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable, of, Subscription, throwError} from 'rxjs';
import {retry, catchError} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {Categoria} from "../models/categoria";
import {Pagination} from "../models/response/pagination";
import {Response} from "../models/response/response";
import {Produto} from "../models/produto";
import {Token} from "../models/token";
import {Carrinho, Item} from "../models/carrinho";

@Injectable()
export class CarrinhoService {


  private carrinhoSubject: BehaviorSubject<Carrinho> =  new BehaviorSubject<Carrinho>(new Carrinho());

  public carrinho: Observable<Carrinho> = this.carrinhoSubject.asObservable();

  url = environment.API_URL

  // injetando o HttpClient
  constructor(private httpClient: HttpClient) {
    let cart = localStorage.getItem("cart");
    if(cart){
      this.carrinhoSubject.next(JSON.parse(cart))
    }
  }

  // Headers
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  }

  saveProduto(): Observable<Response<Carrinho>> {
    let data = this.carrinhoSubject.getValue();
    return this.httpClient.post<Response<Carrinho>>(`${this.url}/Carrinho`, JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(2)
      )
  }

  addProduto(produto: Produto, qtd: number = 1) {
    let data = this.carrinhoSubject.getValue();
      let index = data.carrinho.findIndex(x => x.produto == produto.id);
      if (index !== -1) {
        data.carrinho[index].quantidade = data.carrinho[index].quantidade + qtd;
      } else {
        let item = new Item();
        item.quantidade = qtd;
        item.produto = produto.id;
        data.carrinho.push(item)
      }
      this.setProdutos(data);
      this.carrinhoSubject.next(data);
    this.setProdutos(data);
  }

  removeProduto(produto: Produto) {
      let data = this.carrinhoSubject.getValue();
      let index = data.carrinho.findIndex(x => x.produto == produto.id);
      if (index !== -1) {
        if (data.carrinho[index].quantidade == 1) {
          data.carrinho.splice(index, 1);
        } else {
          data.carrinho[index].quantidade--;
        }
      }
      this.setProdutos(data);
      this.carrinhoSubject.next(data);
  }

  setProdutos(data:any) {
    localStorage.setItem('cart', JSON.stringify(data));
  }

  esvaziarCarrinho(){
    this.carrinhoSubject.next(new Carrinho())
    localStorage.removeItem('cart');
  }
  getPreviewCarrinho() :Observable<Response<Produto[]>>{
    let ids = this.carrinhoSubject.getValue().carrinho.map(value => value.produto)
    return this.httpClient.post<Response<Produto[]>>(`${this.url}/Carrinho/preview`, JSON.stringify(ids), this.httpOptions)
      .pipe(
        retry(2)
      )
  }
}
