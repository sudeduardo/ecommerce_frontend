
import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Cliente } from '../models/cliente';
import {Response} from "../models/response/response";
import { environment } from 'src/environments/environment';
import {Pagination} from "../models/response/pagination";
import {Categoria} from "../models/categoria";

@Injectable()
export class MasterService {

  url = environment.API_URL

  // injetando o HttpClient
  constructor(private httpClient: HttpClient) { }

  // Headers
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  getMasters(
    pageIndex: number = 1,
    pageSize: number = 10,
    nome:string|null = null
  ): Observable<Pagination<Cliente>> {

    let params = new HttpParams()
      .append('PageNumber', `${pageIndex}`)
      .append('PageSize', `${pageSize}`)

    return this.httpClient
      .get<Pagination<Cliente>>(`${this.url}/Master${nome ? `/${nome}`: ''}`, {params})
      .pipe(retry(2),
        catchError(this.handleError));
  }

  getMasterById(id: number): Observable<Cliente> {
    return this.httpClient.get<Cliente>(`${this.url}/Master${id}`)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  saveMaster(cliente: Cliente): Observable<Response<Cliente>> {
    return this.httpClient.post<Response<Cliente>>(`${this.url}/Master`, JSON.stringify(cliente), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  updateCliente(cliente: Cliente): Observable<Cliente> {
    return this.httpClient.put<Cliente>(this.url + '/' + cliente.id, JSON.stringify(cliente), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  deleteMaster(cliente: Cliente) {
    return this.httpClient.delete<Response<Cliente>>(this.url + '/Master/' + cliente.id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
