
import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {Response} from "../models/response/response";
import { environment } from 'src/environments/environment';
import {Produto} from "../models/produto";
import {Categoria} from "../models/categoria";
import {Pagination} from "../models/response/pagination";
import {Venda} from "../models/venda";

@Injectable()
export class VendaService {

  url = environment.API_URL

  // injetando o HttpClient
  constructor(private httpClient: HttpClient) { }

  // Headers
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  getVendas(
    pageIndex: number = 1,
    pageSize: number = 10
    , dataInicio: Date|null, dataFim: Date|null): Observable<Pagination<Venda>> {

    let params = new HttpParams()
      .append('PageNumber', `${pageIndex}`)
      .append('PageSize', `${pageSize}`)

    if(dataInicio && dataFim){
      params = params.append('inicio',dataInicio.toDateString()).append('fim',dataFim.toDateString());
    }

    return this.httpClient
      .get<Pagination<Venda>>(`${this.url}/Carrinho`, {params})
      .pipe(retry(2),
        catchError(this.handleError));
  }

  getVendasByUser(
    id:string,
    pageIndex: number = 1,
    pageSize: number = 10
  ): Observable<Pagination<Venda>> {

    let params = new HttpParams()
      .append('PageNumber', `${pageIndex}`)
      .append('PageSize', `${pageSize}`)

    return this.httpClient
      .get<Pagination<Venda>>(`${this.url}/Carrinho/Cliente/${id}`, {params})
      .pipe(retry(2),
        catchError(this.handleError));
  }
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };


}
