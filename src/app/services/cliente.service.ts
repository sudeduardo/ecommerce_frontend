
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Cliente } from '../models/cliente';
import { Response } from '../models/response/response';

import { environment } from 'src/environments/environment';
import {Endereco} from "../models/endereco";
import {Token} from "../models/token";

@Injectable()
export class ClienteService {

  url = environment.API_URL

  // injetando o HttpClient
  constructor(private httpClient: HttpClient) { }

  // Headers
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  getEnderecoClienteById(id:string) : Observable<Response<Endereco>>{
    return this.httpClient.get<Response<Endereco>>(`${this.url}/Clientes/${id}/endereco`)
      .pipe(
        retry(1),
        catchError(this.handleError))
  }

  getClientes(): Observable<Cliente[]> {
    return this.httpClient.get<Cliente[]>(this.url)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  getClienteById(id: string): Observable<Response<Cliente>> {
    return this.httpClient.get<Response<Cliente>>(`${this.url}/Clientes/${id}`)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  saveCliente(cliente: Cliente): Observable<{token: string}> {
    return this.httpClient.post<{token: string}>(`${this.url}/Clientes`, JSON.stringify(cliente), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  saveEndereco(id:number,endereco:Endereco):Observable<Response<Endereco>>{
    return this.httpClient.put<Response<Endereco>>(`${this.url}/Clientes/${id}/endereco`,JSON.stringify(endereco),this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  updateCliente(cliente: Cliente): Observable<Response<Cliente>> {
    return this.httpClient.put<Response<Cliente>>(`${this.url}/Clientes`, JSON.stringify(cliente), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  deleteCliente(cliente: Cliente) {
    return this.httpClient.delete<Cliente>(this.url + '/' + cliente.id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
