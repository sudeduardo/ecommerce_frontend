
import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {Response} from "../models/response/response";
import { environment } from 'src/environments/environment';
import {Produto} from "../models/produto";
import {Categoria} from "../models/categoria";
import {Pagination} from "../models/response/pagination";

@Injectable()
export class ProdutoService {

  url = environment.API_URL

  // injetando o HttpClient
  constructor(private httpClient: HttpClient) { }

  // Headers
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  getProdutos(
    pageIndex: number = 1,
    pageSize: number = 10,
    titulo:string|null = null
  ): Observable<Pagination<Produto>> {

    let params = new HttpParams()
      .append('PageNumber', `${pageIndex}`)
      .append('PageSize', `${pageSize}`)

    if(titulo){
      params = params.append('titulo',titulo)
    }
    return this.httpClient
      .get<Pagination<Produto>>(`${this.url}/Produto`, {params})
      .pipe(retry(2),
        catchError(this.handleError));
  }

  getProdutoById(id: string): Observable<Response<Produto>> {
    return this.httpClient.get<Response<Produto>>(`${this.url}/Produto/${id}`)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  saveProduto(produto:FormData): Observable<Response<Produto>> {

    const formData = new FormData();
    Object.entries(produto).forEach(data => {
      const [key, value] = data;
      formData.append(key,value)
    })

    return this.httpClient.post<Response<Produto>>(`${this.url}/Produto`, formData)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };

  updateProduto(id:string,produto:FormData) {
    const formData = new FormData();
    formData.append('id',id);
    Object.entries(produto).forEach(data => {
      const [key, value] = data;
      formData.append(key,value)
    })

    return this.httpClient.put<Response<Produto>>(`${this.url}/Produto`, formData)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  deleteProduto(produto: Produto) {
    return this.httpClient.delete<Response<string>>(`${this.url}/Produto/${produto.id}`, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }
}
