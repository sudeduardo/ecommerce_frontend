import { Component, OnInit } from '@angular/core';
import {Token} from "../../models/token";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-menu-profile',
  templateUrl: './menu-profile.component.html',
  styleUrls: ['./menu-profile.component.scss']
})
export class MenuProfileComponent implements OnInit {

  user: Token;
  constructor( private authService: AuthService, private router: Router) {
    this.authService.currentUser.subscribe(x => {
      this.user = x
    });
  }

  visible: boolean;

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
  }

}
