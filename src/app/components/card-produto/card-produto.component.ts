import {Component, Input, OnInit} from '@angular/core';
import {Produto} from "../../models/produto";
import {environment} from "../../../environments/environment";
import {CarrinhoService} from "../../services/carrinho.service";

@Component({
  selector: 'app-card-produto',
  templateUrl: './card-produto.component.html',
  styleUrls: ['./card-produto.component.scss']
})
export class CardProdutoComponent implements OnInit {

  @Input()
  produto:Produto;

  constructor(private carrinhoService:CarrinhoService) { }

  ngOnInit(): void {
  }

  addToCart(){
    this.carrinhoService.addProduto(this.produto,1);
  }

  getImageUrl(){
   return `${environment.API_URL}${this.produto.imagem}`
  }

}
