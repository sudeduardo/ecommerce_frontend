import {Component, Input, OnInit} from '@angular/core';
import {CarrinhoService} from "../../services/carrinho.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  constructor(private carrinhoService:CarrinhoService) {

  }

  @Input()
  public color:any;

  carrinho:number;
  ngOnInit(): void {
    this.carrinhoService.carrinho.subscribe(data =>{
      console.log(data)
      this.carrinho = data.carrinho.reduce((previousValue, currentValue) => previousValue + currentValue.quantidade,0)
    })

  }

}
