import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from "./pages/login/login.component";
import {SingupComponent} from "./pages/singup/singup.component";
import {AuthMasterGuard} from "./guard/auth-master.guard";
import {AuthClienteGuard} from "./guard/auth-cliente.guard";
import {IndexHomeComponent} from "./pages/home/index/index.component";
import {LoginLayoutComponent} from "./layouts/login/login-layout.component";
import {HomeLayoutComponent} from "./layouts/home/home-layout.component";
import {ProfileLayoutComponent} from "./layouts/profile/profile-layout.component";
import {IndexProfileComponent} from "./pages/profile/index/index.component";
import {AdminLayoutComponent} from "./layouts/admin/admin-layout.component";
import {IndexAdminComponent} from "./pages/admin/index/index.component";
import {IndexCategoriasComponent} from "./pages/admin/categorias/index/index.component";
import {RelatoriosComponent} from "./pages/admin/relatorios/relatorios.component";
import {EditCategoriaComponent} from "./pages/admin/categorias/edit/edit.component";
import {CreateProdutoComponent} from "./pages/admin/produtos/create/create.component";
import {EditProdutoComponent} from "./pages/admin/produtos/edit/edit.component";
import {IndexUsuarioAdminComponent} from "./pages/admin/usuarios/master/index/index.component";
import {CreateUsuarioMasterComponent} from "./pages/admin/usuarios/master/create/create.component";
import {IndexProdutoAdminComponent} from "./pages/admin/produtos/index/index.component";
import {PedidosProfileComponent} from "./pages/profile/pedidos/pedidos.component";
import {CategoriasHomeComponent} from "./pages/home/categorias/categorias.component";
import {CarrinhoHomeComponent} from "./pages/home/carrinho/carrinho.component";


const routes: Routes = [
  { path: '',
    component: HomeLayoutComponent,
    children:[
      {
        path: '',
        component: IndexHomeComponent
      },
      {
        path: 'categoria/:id',
        component: CategoriasHomeComponent
      },
      {
        path: 'carrinho',
        component: CarrinhoHomeComponent
      }
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'singup',
        component: SingupComponent
      }
    ]
  },
  {
    path: 'profile',
    component: ProfileLayoutComponent,
    children:[
      {
        path: 'index',
        component: IndexProfileComponent,
      },
      {
        path: 'pedidos',
        component: PedidosProfileComponent,
      }
    ],
    canActivate: [AuthClienteGuard]
  },
  {
    path: 'admin',
    data: {
      breadcrumb: 'Admin'
    },
    component: AdminLayoutComponent,
    children:[
      {
        path: '',
        component: IndexAdminComponent,

      },
      {
        path: 'categorias',
        component: IndexCategoriasComponent,
        data: {
          breadcrumb: 'Categorias'
        },
      },
      {
        path: 'categorias/editar/:id',
        component: EditCategoriaComponent,
        data: {
          breadcrumb: 'Editar Categoria'
        },
      },
      {
        path: 'produtos',
        component: IndexProdutoAdminComponent,
        data: {
          breadcrumb: 'Produtos'
        },
      },
      {
        path: 'produtos/novo',
        component: CreateProdutoComponent,
        data: {
          breadcrumb: 'Novo Produto'
        },
      },
      {
        path: 'produtos/editar/:id',
        component: EditProdutoComponent,
        data: {
          breadcrumb: 'Editar Produto'
        },
      },
      {
        path: 'usuarios/master',
        component: IndexUsuarioAdminComponent,
        data: {
          breadcrumb: 'Usuarios Master'
        },
      },
      {
        path: 'usuarios/master/novo',
        component: CreateUsuarioMasterComponent,
        data: {
          breadcrumb: 'Criar Usuario Master'
        },
      },
      {
        path: 'relatorios',
        component: RelatoriosComponent,
        data: {
          breadcrumb: 'Relatórios'
        },
      }
    ],
    // canActivate: [AuthMasterGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
