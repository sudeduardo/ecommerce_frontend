export class Endereco {


  constructor(id: number = null, rua: string = "", numero: string = "", complemento: string = "", bairro: string="", cidade: string="") {
    this.id = id;
    this.rua = rua;
    this.numero = numero;
    this.complemento = complemento;
    this.bairro = bairro;
    this.cidade = cidade;
  }

  id?: number;
  cep?: string;
  rua?: string;

  numero?: string;

  complemento?: string;

  bairro?: string;

  cidade?: string
}
