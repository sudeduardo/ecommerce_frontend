import {Categoria} from "./categoria";

export class Produto {

    id: number;

    titulo: string;

    descricao: string;

    imagem: string;

    valor:number;

    categoriaId?:number;

    categoria?:Categoria

}
