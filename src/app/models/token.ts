import {Categoria} from "./categoria";

export class Token {
  unique_name?: string;
  role?: string;
  nameid?: string;
  nbf?: number;
  exp?: number;
  iat?: number;
}
