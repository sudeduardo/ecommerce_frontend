import {Cliente} from "./cliente";
import {Produto} from "./produto";

export class ItemDeVenda {
  id:number;

  produto:Produto;

  valorProduto:number;
}

export class Venda {
  id:number;

  cliente:Cliente;

  itemDeVendas:ItemDeVenda[];

  dataCompra:Date;

}
