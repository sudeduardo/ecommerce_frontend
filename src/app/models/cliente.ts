import { Perfil } from "./perfil";
import {Endereco} from "./endereco";

export class Cliente {


  constructor(id: number = null, nome: string= null, dataNasc: Date= null, email: string= null, senha: string= null, endereco: Endereco= null, perfil: Perfil= null) {
    this.id = id;
    this.nome = nome;
    this.dataNasc = dataNasc;
    this.email = email;
    this.senha = senha;
    this.endereco = endereco;
    this.perfil = perfil;
  }

  id: number;

    nome: string;

    dataNasc: Date;

    email: string;

    senha: string;

    endereco:Endereco;

    perfil: Perfil
}
