export class Carrinho {
  carrinho:Item[]

  constructor(carrinho: Item[] = []) {
    this.carrinho = carrinho;
  }
}

export class Item{
  produto:number;
  quantidade:number;
}
