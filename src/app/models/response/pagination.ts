export interface Pagination<T> {
  pageNumber: number;
  pageSize: number;
  firstPage: string;
  lastPage: string;
  totalPages: number;
  totalRecords: number;
  nextPage?: any;
  previousPage: string;
  data: T[];
  succeeded: boolean;
  errors?: any;
  message?: any;
}
